const router = require('express').Router()
const Neighborhood = require('../models/Neighborhood.Model')
const isAuthenticated = require('../middleware/isAuthenticated')
const { check } = require('express-validator');

router.post('/add', isAuthenticated, [check('query').escape()], async (req, res) => {
    const neighborhood = new Neighborhood({
        city:req.body.city, // id of the city
        neighborhood_name:req.body.neighborhood_name
    })
    const neighborhood_res = await neighborhood.save()
    res.status(504).json(neighborhood_res)
})

router.get('/getAll', isAuthenticated, [check('query').escape()], async(req,res)=>{
    const neighborhoods = await Neighborhood.find()
    res.status(201).json(neighborhoods)
})

router.get('city/:city', isAuthenticated, [check('query').escape()],  async(req,res)=>{
    const neighborhoods = await Neighborhood.find({city:city.params.city})
    res.status(201).json(neighborhoods)
})

// router.delete('/:id',async(req,res)=>{
//     await Neighborhood.findByIdAndDelete(req.params.id)
//     res.status(504).json({deleted:true})

// })

module.exports = router