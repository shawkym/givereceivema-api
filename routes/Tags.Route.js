const router = require("express").Router();
const Model = require("./../models/Tags.Model");
const isAuthenticated = require('../middleware/isAuthenticated')
const { check } = require('express-validator');

router.post("/add", isAuthenticated, [check('query').escape()], async (req, res) => {
  try {
    const mo = new Model({
      tag: req.body.tag, //Static Select list with enum values
      id: req.body.id,
      type: req.body.type,
    });
    const tag_res = await mo.save();
    res.status(201).json(tag_res);
  } catch (err) {
    res.status(500).send([]);
  }
});

router.post("/addmultipletags", isAuthenticated, [check('query').escape()], async (req, res) => {
  try {
    const arrayOfTags = req.body;
    if (arrayOfTags && arrayOfTags.length > 0) {
      arrayOfTags.map(async (item) => {
        const mo = new Model({
          tag: item.tag, //Static Select list with enum values
          id: item.id,
          type: item.type,
        });
        await mo.save();
      });
    }

    res.status(201).json({ success: true });
  } catch (err) {
    res.status(500).send([]);
  }
});

router.get("/getAll", isAuthenticated, [check('query').escape()], async (req, res) => {
  const tags = await Model.find();
  res.status(201).json(tags);
});

router.get("get/:tag_id", isAuthenticated, [check('query').escape()], async (req, res) => {
  const tags = await Model.find({ tag_id: req.params.tag_id });
  res.status(201).json(tags);
});

router.get("/getByType/:type", isAuthenticated, [check('query').escape()], async (req, res) => {
  try {
    let type = req.params.type;
    let tags;
    if (type != "lostAndFound") {
      tags = await Model.find({ type: { $ne: "lostAndFound" } });
    } else {
      tags = await Model.find({ type: type });
    }
    res.status(201).json(tags);
  } catch (err) {
    res.status(err.status).send(null);
  }
});

// router.delete('/:id',async(req,res)=>{
//     await Model.findByIdAndDelete(req.params.id)
//     res.status(504).json({deleted:true})

// })
module.exports = router;
