const Controller = require('../controllers/CardController')

function CardRoutes(cardType) {
    const isAuthenticated = require('../middleware/isAuthenticated')
    const express = require('express');
    const router = express.Router();
    const { validationResult } = require('express-validator');
    // check used with esacpe to sanitize requests in protection effort to code injection
    const { check } = require('express-validator');
    // Create Card Model in DB with provided name
    require("../models/Card.Model")(cardType);

    // Generate routes for card model 
    router.get("/", isAuthenticated, [check('query').escape()], function (req, res) {
        const Controller = require('../controllers/CardController')
        // Generate get all cards route for card model 
        Controller.getAll(cardType, req, res);
    });

    // Generate filter cards route for card model 
    router.get("/filter", isAuthenticated, [check('query').escape()], function (req, res) {
        const Controller = require('../controllers/CardController')
        // Generate filter cards route for card model 
        Controller.getWithFilters(cardType, req, res);
    });

    // Generate paginator route for cards model 
    router.get("/offset/:page/", isAuthenticated, [check('query').escape()], function (req, res) {
        const Controller = require('../controllers/CardController')
        return Controller.getWithOffset(cardType, req, res);
    });

    // Generate post route for adding new cards to DB 
    router.post("/", isAuthenticated, [check('query').escape()], function (req, res) {
        const Controller = require('../controllers/CardController')
        Controller.create(cardType, req, res);
    });

    // To be Implemented with Auth
    // router.post("/delete", [check('query').escape()], function (req, res) {
    //     Controller.deleteOne(cardType, req, res);
    // });
    
    // Flag it as fulfilled
    router.post("/fulfill", isAuthenticated, [check('query').escape()], function (req, res) {
        Controller.fulfill(cardType, req, res);
    });

    //Route for feedbacking a card in DB
    router.post("/feedback", isAuthenticated, [check('query').escape()], function (req, res) {
        const Controller = require('../controllers/CardController')
        Controller.feedback(cardType, req, res);
    });
    //Route for flagging a card in DB
    router.post("/flag", isAuthenticated, [check('query').escape()], function (req, res) {
        const Controller = require('../controllers/CardController')

        Controller.flag(cardType, req, res);
    });
    return router;
}

module.exports = CardRoutes;
