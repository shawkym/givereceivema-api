const express = require('express');
const Controller = require('../controllers/MissingController')
const router = express.Router();
const isAuthenticated = require('../middleware/isAuthenticated')
const { check } = require('express-validator');

//Blogs routes
router.get("/", isAuthenticated, [check('query').escape()], Controller.getAll);

router.post("/", isAuthenticated, [check('query').escape()], Controller.create);

router.post("/delete", isAuthenticated, [check('query').escape()], Controller.deleteOne);
router.post("/feedback", isAuthenticated, [check('query').escape()], Controller.feedback);
router.post("/flag", isAuthenticated, [check('query').escape()], Controller.flag);

// router.put("/", Controller.update)


// router.delete("/:id",Controller.deleteOne);
// router.delete("/deleteAll",Controller.deleteAll);


module.exports = router;