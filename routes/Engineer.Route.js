const router = require('express').Router()
const Engineer = require('../models/Engineer.Model')
const isAuthenticated = require('../middleware/isAuthenticated')
const { check } = require('express-validator');

router.post('/add', isAuthenticated, [check('query').escape()], async (req, res) => {
    const eng = new Engineer({
        engineer_name:req.body.engineer_name,
        phone_number:req.body.phone_number,
        city:req.body.city,
        neighborhood:req.body.neighborhood
    })
    const eng_res = await eng.save()
    res.status(200).json(eng_res)
})

router.get('/getAll', isAuthenticated, [check('query').escape()], async(req,res)=>{
    const engs = await Engineer.find()
    res.status(201).json(engs)
})

// router.delete('/:id',async(req,res)=>{
//     await City.findByIdAndDelete(req.params.id)
//     res.status(504).json({deleted:true})

// })

module.exports = router