const router = require('express').Router()
const City = require('../models/City.Model')
const isAuthenticated = require('../middleware/isAuthenticated')
const { check } = require('express-validator');

router.post('/add', isAuthenticated, [check('query').escape()], async (req, res) => {
    const city = new City({
        city_name:req.body.city_name
    })
    const city_res = await city.save()
    res.status(504).json(city_res)
})

router.get('/getAll', isAuthenticated, [check('query').escape()], async(req,res)=>{
    const cities = await City.find()
    res.status(201).json(cities)
})

// router.delete('/:id',async(req,res)=>{
//     await City.findByIdAndDelete(req.params.id)
//     res.status(504).json({deleted:true})

// })

module.exports = router