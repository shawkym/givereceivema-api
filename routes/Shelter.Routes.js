const express = require('express');
const Controller = require('../controllers/ShelterController')
const router = express.Router();
const isAuthenticated = require('../middleware/isAuthenticated')
const { check } = require('express-validator');

router.get("/", isAuthenticated, [check('query').escape()], Controller.getAll);

module.exports = router;