const express = require("express");
const morgan = require("morgan");
const mongoose = require("mongoose");
const CardRoutes = require('./routes/Card.Routes')
const ShelterRoutes = require('./routes/Shelter.Routes')
const CityRoutes = require('./routes/City.Route')
// const NeighborhoodRoutes = require('./routes/Neighborhood.Route')
const TagsRoutes = require('./routes/Tags.Route')
const MissingRoute = require('./routes/Missing.Route')
const EngineerRoute = require('./routes/Engineer.Route')

const cors = require("cors")
require("dotenv").config();

const app = express();
app.use(cors())

//PLEASE DO NOT PUT ANY STRINGS HERE, USE ENVIRONMENT VARIABLES 
const dbURI = process.env.DB_URI || process.env.DB_URI_TEST;
mongoose
  .connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then((res) => {
    console.log("succes on port 5931");
    app.listen(5931);
  })
  .catch((err) => {
    console.log("error: ", err);
  });

//middleware and static files , specify the folder name
app.use(express.urlencoded({ extended: true }));
app.use(morgan("dev"));
app.use(express.json());
//Main route
app.get("/", (req, res) => {
  res.send("Give and receive API ");
})

// const corsOptions = {
//   origin: ['*'],
//   methods: ['*'],
//   allowedHeaders: ['*'],
// }
const corsOptions = {
  origin: ['https://front-ma.onrender.com/','https://drmaroc.fr/','http://drmaroc.fr/'],
  methods: ['GET','POST','HEAD'],
  allowedHeaders: ['Content-Type', 'Authorization'],
}

app.use(cors(corsOptions))

//routes
//PLEASE DO NOT CHANGE THOSE BECAUSE THEY ARE USED IN THE FRONT NOW ! THANKS
app.use('/receive', new CardRoutes('receive'))
app.use('/give', new CardRoutes('give'))
app.use('/city', CityRoutes)
app.use('/tags', TagsRoutes);
app.use('/requirement', TagsRoutes);
app.use('/shelter', ShelterRoutes);
app.use('/lost', new CardRoutes('lost'))
app.use('/reclaim', new CardRoutes('reclaim'))
app.use('/found', new CardRoutes('found'))
app.use('/urgent', new CardRoutes('urgent'))
app.use('/engineer', EngineerRoute)
app.disable('etag');

// app.use('/Neighborhood', NeighborhoodRoutes)

//404
app.use((req, res) => {
  res.status(404).send("<h1> Error 403, Sorry, this API route is not found :( </h1>");
});
