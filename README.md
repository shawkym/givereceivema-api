# Help by GIVE AND GET

## Starting the backend api server:

first clone this repo and then run once:
```
    npm install
```
then you can always start the server by running:
```
    npm start
```

## Structure:
The application start point is App.js which is starting the server on port *5931*. it will not start listening until connecting to the mongodb.
In the app.js there are the main routes and each route is connected to a route file.js
```
    app.use('/receive', ReceiveRoutes)
    app.use('/give', GiveRoutes)
```

Each route is connected to a Controller file. The route file has the routes after the main route in app.js. Example: 
In the giveRoutes.js
```
    router.post("/create", Controller.create);
```
means that the front can access this via 
```
localhost:5931/give/create
```
The request will trigger one of the functions in the controller.

The controller processes the data based on the model connecting to it. 
Each controller is importing a model schema which is exported in a model.js file inside the model folder

The model schema have the schema of each collection.
