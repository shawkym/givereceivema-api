const express = require('express');
const otp = require('simpleotp');
const totp = new otp.Totp();

module.exports = (req, res, next) => {
    const authHeader = req.get('Authorization');
        // validate the token
    const data = {token: authHeader, secret:process.env.SECRET,seconds :Date.now()/1000}
    const valid = totp.validate(data)
    if (!authHeader || authHeader == null) {
        const error = new Error("token not found");
        error.status = 401;
        throw error;
    }
    if (!valid){
        const error = new Error("token not valid");
        error.status = 401;
        throw error;
    }
    next()
};