const mongoose = require('mongoose')
const EngineerSchema = new mongoose.Schema({
    name:{type:String,required:true},
    contact:{type:String,required:true},
    city:{type:String,required:true},
    offer:{type:String,required:true}
}, {
    timestamps: true
})
module.exports = mongoose.model("Engineer", EngineerSchema)
