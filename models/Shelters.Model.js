const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const shelterSchema = new Schema(
    {
        name: {
            type: String,
            required: true,
        },
        contact: {
            type: String,
            required: true,
        },
        city: {
            type: String,
            required: true,
        },
        address: {
            type: String,
            required: true,
        },
        details: {
            type: String,
            required: false
        },
        status: {
            type: String,
            required: false
        },
        tags: {
            type: [String],
            required: false
        }
    },

    { timestamps: true }
);



const shelter = mongoose.model("shelter", shelterSchema);
module.exports = shelter;

