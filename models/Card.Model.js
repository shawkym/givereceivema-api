// Dynamic Model for Cards
module.exports = (modelName) => {

  const mongoose = require("mongoose");

  const Schema = mongoose.Schema;
  
  const schema = new Schema(
    {
      title: {
        type: String,
        required: true,
      },
      details: {
        type: String,
        required: true,
      },
      phoneNumber: {
        type: String,
        required: true,
      },
      city: {
        type: String,
        required: true,
      },
      flagged: {
        type: Number,
        required: true,
        default: 0,
      },
      feedbacked: {
        type: Number,
        required: true,
        default: 0,
      },
      fulfilled: {
        type: Boolean,
        required: true,
        default: false,
      },
      tags: {
        type: [String],
      },
    },
    { timestamps: true }
  );
 return new mongoose.model(`${modelName}`, schema);
}