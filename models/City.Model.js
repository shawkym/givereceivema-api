const mongoose = require('mongoose')
const CitySchema = new mongoose.Schema({
    city_name:{
        type:String,
        required:true,
        unique: true
    },
    city_name_ar:{
        type:String,
        required:true,
        unique: true
    },
    city_name_fr:{
        type:String,
        required:true,
        unique: true
    }
}, {
    timestamps: false
})
module.exports = mongoose.model("City", CitySchema)
