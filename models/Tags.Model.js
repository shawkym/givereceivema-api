const mongoose = require('mongoose')
const TagsSchema = new mongoose.Schema({
    tag: {
        type: String,
        required: true,
    },
    id: {
        type: Number,
        required: true
    },
    type: {
        type: String,
        required: true,
    },
}, {
    timestamps: true
})

module.exports = mongoose.model("Tags", TagsSchema)