const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MissingSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    details: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: false,
    },
    status: {
      type: String,
      required: false,
    },
    contactNumber: {
      type: String,
      required: true,
    },
    city:{
      type:String,
      required: true
    },
    found:{
      type:Boolean,
      required: false
    }
  },

  { timestamps: true }
);



const Missing = mongoose.model("Missing", MissingSchema);
module.exports = Missing;

