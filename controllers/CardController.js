// import Model from model file

const { default: mongoose } = require("mongoose");

// Get all un-fulfilled documents sorted by createdAt
const getAll = async (cardType, req, res) => {
  Model = mongoose.model(cardType);
  try {
    const rrr = await Model.find().sort({ createdAt: -1 });
    const result = rrr.filter((ele) => !ele.fulfilled);
    res.json(result);
  } catch (ex) {
    console.log("error: ", ex);
    res.status(504).send([]);
  }
};

// Get documents with pagination
const getWithOffset = async (cardType, req, res) => {
  Model = mongoose.model(cardType);
  try {
    const pageSize = 10;
    let page = req.params.page; // current page number
    let withFulfilled = req.query.includeFulfilled; // current page number
    if (!page || page == 0) {
      page = 1;
    }
    const total = await Model.countDocuments({}); // total number of documents
    numberOfPages = null;
    division = total / pageSize;
    const remainder = total % pageSize;
    numberOfPages = remainder !== 0 ? Math.floor(division + 1) : division; // calculate total number of pages based on pageSize and total documents
    const result = await Model.find(withFulfilled ? {} : { fulfilled: false })
      .sort({ createdAt: -1 })
      .skip((page - 1) * pageSize)
      .limit(pageSize);
    res.json({
      total,
      numberOfPages,
      currentPage: page,
      resultArray: result,
    });
  } catch (ex) {
    console.log("error: ", ex);
    res.status(504).send([]);
  }
};

// Create new document
const create = async (cardType, req, res) => {
  Model = mongoose.model(cardType);
  if (req.body.tags) {
    if (typeof req.body.tags === "string" && req.body.tags.includes("|")) {
      req.body.tags = req.body.tags.split("|");
      req.body.tags.forEach((element) => {
        element = element.trim();
      });
    }
  }
  const obj = new Model(req.body);
  try {
    const result = await obj.save();
    res.send(result);
  } catch (err) {
    console.log("error: ", err);
    res.status(504).send(null);
  }
};

// Set a document as fulfilled
const fulfill = async (cardType, req, res) => {
  const id = req.body._id;
  try {
    const result = await mongoose
      .model(cardType)
      .findByIdAndUpdate(id, { fulfilled: true });
    res.send(result);
  } catch (ex) {
    console.log("error: ", ex);
    res.status(504).send(null);
  }
};

// Increase the feedback count of a document by 1
const feedback = async (cardType, req, res) => {
  const id = req.body._id;
  try {
    const result = await mongoose
      .model(cardType)
      .findByIdAndUpdate(id, { $inc: { feedbacked: 1 } }, { new: true });
    res.send(result);
  } catch (ex) {
    console.log("error: ", ex);
    res.status(504).send(null);
  }
};

// Get documents based on filter conditions
const getWithFilters = async (cardType, req, res) => {
  try {
    const { title, city, phoneNumber, details, tags, includeFulfilled } =
      req.query;
    const conditions = [];

    // If there are filters based on title, details, or phoneNumber, combine conditions using $or operator
    if (title || details || phoneNumber) {
      const titleConditions = title ? { title: { $in: title } } : {};
      const detailWords = details ? details.split(" ") : [];
      const detailConditions =
        detailWords.length > 0
          ? {
              details: {
                $all: detailWords.map((word) => new RegExp(word, "i")),
              },
            }
          : {};
      const phoneNumberConditions = phoneNumber
        ? { phoneNumber: { $regex: `\\b\\d*${phoneNumber}\\d*\\b` } }
        : {};
      conditions.push({
        $or: [titleConditions, detailConditions, phoneNumberConditions],
      });
    }

    // If there are filters based on city or tags, combine conditions using $and operator
    if (city || tags) {
      const otherConditions = [];
      if (city) {
        otherConditions.push({ city: { $in: JSON.parse(city) } });
      }
      if (tags) {
        otherConditions.push({ tags: { $in: JSON.parse(tags) } });
      }
      conditions.push({ $and: otherConditions });
    }
    includeFulfilled == "true" ? "" : conditions.push({ fulfilled: false });
    // Combine all conditions using $and operator to construct the final query object
    const query = conditions.length > 0 ? { $and: conditions } : {};

    // Perform the find operation with the constructed query object and return the results
    const result = await mongoose.model(cardType).find(query).lean();
    res.json(result);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server error" });
  }
};

// Increase the flag count of a document by 1
const flag = async (cardType, req, res) => {
  const id = req.body._id;
  try {
    const result = await mongoose
      .model(cardType)
      .findByIdAndUpdate(id, { $inc: { flagged: 1 } }, { new: true });
    res.send(result);
  } catch (ex) {
    console.log("error: ", ex);
    res.status(504).send(null);
  }
};

// Export all the functions as module
module.exports = {
  getAll,
  create,
  feedback,
  flag,
  fulfill,
  getWithFilters,
  getWithOffset,
};
