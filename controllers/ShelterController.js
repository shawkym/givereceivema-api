const Model = require("../models/Shelters.Model");

const getAll = async (req, res) => {
  try {
    const result = await Model.find().sort({ createdAt: -1 });
    res.json(result);

  } catch (ex) {
    console.log("error: ", ex);
    res.status(504).send([]);
  }
};

const getWithFilters = async (req, res) => {
  try {
    const queryString = req.params.queryString;
    const result = await Model.find({
      $or: [
        { name: { $regex: `.*${queryString}.*`, $options: 'i' } },
        { details: { $regex: `.*${queryString.substring(0, 12)}.*`, $options: 'i' } },
        { city: { $regex: `.*${queryString}.*`, $options: 'i' } },
        { contact: { $regex: `.*${queryString}.*`, $options: 'i' } }
      ]
    })
    res.json(result);

  } catch (ex) {
    console.log("error: ", ex);
    res.status(504).send([]);
  }
};

module.exports = {
  getAll,
  getWithFilters,
};
