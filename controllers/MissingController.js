const Model = require("../models/Missing.Model");

const getAll = async (req, res) => {
  try {
    const rrr = await Model.find().sort({createdAt: -1});
    const result = rrr.filter(ele => !ele.fulfilled);
    res.json(result);

  } catch (ex) {
    console.log("error: ", ex);
    res.status(504).send([]);
  }
};
const create = async (req, res) => {
  const obj = new Model(req.body);
  try {
    const result = await obj.save();
    res.send(result);
  } catch (err) {
    console.log("error: ", err);
    res.status(504).send(null);
  }
};

const deleteOne = async (req, res) => {
  const id = req.body._id;
  try {
    const result = await Model.findByIdAndUpdate(id, {fulfilled: true});
    res.send(result);
  } catch (ex) {
    console.log("error: ", ex);
    res.status(504).send(null);
  }
};

const feedback = async (req, res) => {
  const id = req.body._id;
  try {
    const result = await Model.findByIdAndUpdate(id, {$inc: { feedbacked: 1 } }, { new: true });
    res.send(result);
  } catch (ex) {
    console.log("error: ", ex);
    res.status(504).send(null);
}
};

const flag = async (req, res) => {
    const id = req.body._id;
    try {
      const result = await Model.findByIdAndUpdate(id, {$inc: { flagged: 1 } }, { new: true });
      res.send(result);
    } catch (ex) {
      console.log("error: ", ex);
      res.status(504).send(null);
    }
};

const getWithFilters = async (req, res) => {
  try {
    const queryString = req.params.queryString;
    const result = await Model.find({
      $or: [
        { name: { $regex: `.*${queryString}.*`, $options: 'i' } },
        { details: { $regex: `.*${queryString.substring(0, 12)}.*`, $options: 'i' } },
        { phoneNumber: { $regex: `.*${queryString}.*`, $options: 'i' } },
        { city: { $regex: `.*${queryString}.*`, $options: 'i' } },
        { tags: { $regex: `.*${queryString}.*`, $options: 'i' } }
      ]
    })
    res.json(result);

  } catch (ex) {
    console.log("error: ", ex);
    res.status(504).send([]);
  }
};

    module.exports = {
      getAll,
      create,
      feedback,
      getWithFilters,
      flag,
      deleteOne
    };
